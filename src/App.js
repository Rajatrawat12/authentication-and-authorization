import React, { useEffect, useState } from "react";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Login from "./Components/Login";
import Home from "./Components/Home";
import Products from "./Components/Products";
import Carts from "./Components/Carts";
import ProductsdetailsPage from "./Components/ProductDetailsPage";
import PageNotFound from "./Components/PageNotFound";
import { createContext } from "react";

export const UserContext = createContext();

function App() {
  const [clickedProduct, setClickedProduct] = useState({});
  const [newCartItems, setNewCartItems] = useState([]);
  const [quantity, setQuantity] = useState({});
  const cartLength = newCartItems.length
  

  return (
    <UserContext.Provider value={{clickedProduct, cartLength }}>
      <BrowserRouter>
        <Routes>
          <Route exact path="/login" element={<Login />} />
          <Route exact path="/" element={<Home />} />
          <Route
            exact
            path="/Products"
            element={<Products setClickedProduct={setClickedProduct} />}
          />
          <Route exact path="/Carts" element={<Carts newCartItems={newCartItems} setNewCartItems={setNewCartItems} quantity={quantity} setQuantity={setQuantity}/>} />
          <Route
            exact
            path="/Product/details/:id"
            element={<ProductsdetailsPage setNewCartItems={setNewCartItems} />}
          />
          <Route exact path="/*" element={<PageNotFound />} />
        </Routes>
      </BrowserRouter>
    </UserContext.Provider>
  );
}

export default App;

  

