import { useState } from "react";
import { useNavigate } from "react-router-dom"; // Import useNavigate hook
import Cookies from "js-cookie";
import "./Login.css";

const Login = () => {
  const [message, setMessage] = useState('');
  const [userName, setUserName] = useState("");
  const [password, setPassword] = useState("");
  const navigate = useNavigate(); // Get the navigate function from useNavigate
  const [showSubmitErrorMsg, setSubmitErrorMsg] = useState(false);
  const [errorMsg, setErrorMsg] = useState("");
  const storeCredentials = () => {
    const credentials = {
      email: 'Rajat@gmail.com',
      password: 'Rajat@123',
    };
    localStorage.setItem('credentials', JSON.stringify(credentials));
    setMessage('Credentials stored in local storage.');
  };
  
  const enterUserName = (event) => {
    setUserName(event.target.value);
  };

  const enterPassword = (event) => {
    setPassword(event.target.value);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const storedCredentials = JSON.parse(localStorage.getItem('credentials'));
    console.log(storedCredentials.email)

    if (storedCredentials && storedCredentials.email === userName && storedCredentials.password === password) {
      setMessage('Login successful');
      navigate("/");
     
      
    }
    else if (userName.trim() === '' && password.trim() === ''){
      setSubmitErrorMsg(true);
      setErrorMsg("Input credentials cannot be empty");
     

    } 
    else if (userName.trim()===""){
      setSubmitErrorMsg(true);
      setErrorMsg("Input username cannot be empty");
      
    }
    else if (password.trim()===""){
      setSubmitErrorMsg(true);
      setErrorMsg("Input password cannot be empty");
    }
    
    else {
   
        setSubmitErrorMsg(true);
        setErrorMsg("Please enter valid credentials")
      setMessage('Login failed');
      
    }
  };

  


  return (
    <div className="LoginFormContainer">
      <img
        className="LoginFormImages"
        src="https://assets.ccbp.in/frontend/react-js/nxt-trendz-login-img.png"
        alt="Login Image"
      />
      <form className="loginFormDetailContainer" onSubmit={handleSubmit}>
        <div className="LogoImageContainer">
          <img
            className="LoginLogo"
            src="https://assets.ccbp.in/frontend/react-js/nxt-trendz-logo-img.png"
            alt="Logo"
          />
        </div>
        <div>
          <label className="usernameText">USERNAME</label>
          <br />
          <input
            type="text"
            placeholder="UserName"
            className="inputText"
            value={userName}
            onChange={enterUserName}
          />
        </div>
        <div>
          <label className="usernameText">PASSWORD</label>
          <br />
          <input
            type="password"
            placeholder="Password"
            className="inputText"
            value={password}
            onChange={enterPassword}
          />
        </div>
        <input type="submit" value="Login" className="LoginButton"  onClick={storeCredentials}/>
        {showSubmitErrorMsg && <p className="error-msg">{errorMsg}</p>}
      </form>
    </div>
  );
};

export default Login;
