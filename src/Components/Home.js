import "./Home.css";
import Header from "./Header";
import {useNavigate}  from "react-router-dom";
const Home = () => {
const navigate = useNavigate();
const navigateToProductPage=()=>{
  
  navigate("/Products");
  }
  return (
    <div>
    <Header/>
      <div className="contentContainer">
        <div className="textContainer">
          <h1 className="heading">Clothes That Get YOU Noticed</h1>
          <p className="fashionText">
            Fashion can be termed as the creativity of people which adds to the
            beauty of a person. It glorifies the personality of an individual
            especially when a person wears new and trendy clothes and applies
            well makeup on her/himself, her/his looks and appearance gets
            changed.As each of us wants to look handsome, beautiful and pretty
            so that people could appreciate us after looking at us and become
            influenced, it is the fashion which helps us in this aspect. Fashion
            has the capability to change life, the thinking, the temperament of
            a person. Everybody in spite of any age or gender follow certain
            trends of fashion which enhances his/her overall personality.
          </p>
          <button className="shopBtn" onClick={navigateToProductPage}>Shop Now</button>
        </div>
        <img src="https://assets.ccbp.in/frontend/react-js/nxt-trendz-home-img.png" />
      </div>
    </div>
  );
};
export default Home;
