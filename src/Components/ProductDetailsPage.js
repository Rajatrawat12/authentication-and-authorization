import React from "react";
import { useParams } from "react-router-dom";
import ReactDOM from "react-dom/client";
import { useNavigate } from "react-router-dom";
import { createContext } from "react";
import { useState, useContext } from "react";
import { UserContext } from "../App";
import "./productDetailsPage.css";
import Header from "./Header";
import Carts from "./Carts";
// export const UserContext = createContext();

const ProductDetailsPage = ({setNewCartItems}) => {
  const { clickedProduct } = useContext(UserContext);
  const { id } = useParams();
  const { rating } = clickedProduct;
  const { rate, count } = rating;
  const navigate = useNavigate();
  const[buttonClicked,setButtonClicked]=useState(false);

  const navigateingback = () => {
    navigate(`/Products`);
  };

  const addToCart = () => {
    setButtonClicked(true);
    setNewCartItems((prevCartItems) => {
      // Check if the clickedProduct already exists in the cart
      const isItemAlreadyInCart = prevCartItems.some(
        (item) => item.id === clickedProduct.id
      );
      // If it's not in the cart, add it; otherwise, keep the cart unchanged
      return isItemAlreadyInCart ? prevCartItems : [...prevCartItems, clickedProduct];
    });
  
  }
    

  
  //  const {image,price,description}=clickedItem

  // Assuming you have access to the individualItem state here

  // If you have individualItem data, you can destructure its properties
  return (
    <div>
      <Header/>
      <h1 className="ProductHeading">Product Details</h1>
      <div className="Productdetailcontainer">
        <img className="productIndividualImage" src={clickedProduct.image} />
        <div className="allDetailsContainer">
          <p className="titleTag">{clickedProduct.title}</p>
          <p className="categoryTag">Category:{clickedProduct.category}</p>
          <p className="priceTag">Price: Rs. {clickedProduct.price} Only</p>
          <p className="rateTag">Rating:{rate}</p>
          <p className="countTag">Rating count:{count}</p>
          <p className="descriptionTag">{clickedProduct.description}</p>
          <button className="btn">Buy Now</button>
          <button
            className={`btn ${buttonClicked ? "added" : ""}`}
            onClick={addToCart}
            disabled={buttonClicked} // Disable the button after clicking
          >
            {buttonClicked ? "Added to Cart" : "Add To Cart"}
          </button>
          <button className="btn" onClick={navigateingback}>
            Go Back
          </button>
        </div>
      </div>
    </div>
   
  );
}
export default ProductDetailsPage;
