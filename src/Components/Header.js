import "../App.js"
import "./Header.css"
import { useContext } from "react";
import {useNavigate}  from "react-router-dom";
import { UserContext } from "../App";
import {} from "../App.js"
const Header=()=>{
  const { cartLength } = useContext(UserContext);
    const navigate = useNavigate(); // Get the navigate function from useNavigate
    const handleHomeClick = () => {
      // Call navigate to navigate to the Home page
      navigate("/");
    };
    const handleProductsClick = () => {
        // Call navigate to navigate to the Home page
        navigate("/Products");
      };
      const handleCartClick = () => {
        // Call navigate to navigate to the Home page
        navigate("/Carts");
      };
      const handleLogoutClick = () => {
        // Call navigate to navigate to the Home page
        navigate("/login");
      };
console.log(cartLength)

return(
<div className="headerContainer">
        <img
          className="headerLogo"
          src="https://assets.ccbp.in/frontend/react-js/nxt-trendz-logo-img.png"
        />
        <div className="optionContainer">
          <p className="headerSection" onClick={handleHomeClick}>Home</p>
          <p className="headerSection" onClick={handleProductsClick}>Products</p>
          <p className="headerSection" onClick={handleCartClick}>Cart<span className="countDiv"> {cartLength}</span> </p>
          
          <button className="logoutBtn"onClick={handleLogoutClick}>Logout</button>
        </div>
      </div>
)
}
export default Header