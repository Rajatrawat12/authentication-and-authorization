import Header from "./Header";
import "./Carts.css";
import { useEffect } from "react";


const Carts = ({ newCartItems, setNewCartItems, quantity, setQuantity}) => {
  // Initialize the quantity state as an object

  // Initialize the quantity object when newCartItems change
  useEffect(() => {
    const initialQuantity = quantity;
    newCartItems.forEach((item) => {
      if (!initialQuantity[item.id]) {
        initialQuantity[item.id] = 1;
      }
    });
    setQuantity(initialQuantity);
    console.log(initialQuantity);
  }, [newCartItems]);

  const decrementCount = (itemId) => {
    if (quantity[itemId] > 1) {
      console.log("hello");
      // Create a copy of the quantity object with decreased quantity for the specific item
      const updatedQuantity = { ...quantity };
      console.log(quantity);
      console.log(updatedQuantity);
      updatedQuantity[itemId] = quantity[itemId] - 1;
      setQuantity(updatedQuantity);
      console.log(updatedQuantity);
    }
  };

  const incrementCount = (itemId) => {
    // Create a copy of the quantity object with increased quantity for the specific item
    const updatedQuantity = { ...quantity };
    updatedQuantity[itemId] = quantity[itemId] + 1;
    setQuantity(updatedQuantity);
  };

  const deleteItemFromCart = (itemId) => {
    // Filter out the item to be deleted from the cart
    const updatedCartItems = newCartItems.filter((item) => item.id !== itemId);
    setNewCartItems(updatedCartItems);
    const newQuantity = {...quantity};
    newQuantity[itemId] = 1; 
    setQuantity(newQuantity)
    console.log(quantity);
  }

  const arrayLength = newCartItems.length;
  return (
    <>
      <Header />
      {arrayLength === 0 ? (
        <div className="cartContainer">
          <img
            className="CartImage"
            src="https://assets.ccbp.in/frontend/react-js/nxt-trendz-cart-img.png"
            alt="Empty Cart"
          />
        </div>
      ) : (
        <div>
          {newCartItems.map((cartItem) => {
            return (
              <div className="custom-div" key={cartItem.id}>
                <img
                  className="cartAddedImage"
                  src={cartItem.image}
                  alt="Product"
                />
                <p className="priceAddedItem">
                  Price: Rs.{cartItem.price.toFixed(2)}
                </p>
                <p className="quantItems">
                  Quantity:{" "}
                  <button onClick={() => decrementCount(cartItem.id)}>
                    {" "}
                    -{" "}
                  </button>{" "}
                  {quantity[cartItem.id] ?? 1}{" "}
                  <button onClick={() => incrementCount(cartItem.id)}>
                    {" "}
                    +{" "}
                  </button>{" "}
                </p>
                
                <p className="totalAddedItem">
                  Total: Rs.
                  {(cartItem.price * (quantity[cartItem.id] ?? 1)).toFixed(2)}
                </p>
                <button
                  className="RemoveCartItem"
                  onClick={() => deleteItemFromCart(cartItem.id)}
                >
                  Delete from Cart
                </button>
              </div>
            );
          })}
        </div>
      )}
    </>
  );
};

export default Carts;
