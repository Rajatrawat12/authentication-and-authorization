import Header from "./Header";
import { useEffect,useState } from "react";
import "./Products.css";
import Data from "./Data";
const Products = ({setClickedProduct}) => {
    const [productData, setProductData] = useState([]);
    const [fetchProductData, setFetchProductData] = useState(true);
    

    useEffect(() => {
        fetch("https://fakestoreapi.com/products")
          .then((response) => response.json())
          .then((data) => {
            setProductData(data); 
            setFetchProductData(false)
            // Update the state with fetched data
          })
          .catch((error) => {
            console.error("Error fetching data:", error);
            setFetchProductData(false)
          });
      }, []);
      console.log(productData)

  return (
    <>
    {}
      <Header />
      <div className="productContainer">
      {fetchProductData ? (
          <p className="fetching">Loading...</p>
        ) : (
        productData.map((item) =>(
        <Data key={item.id} item={item} setClickedProduct={setClickedProduct}/>
        )))
        }
      </div>
    </>
        
  );
};
export default Products;
