import React, { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import "./Data.css";
import ProductDetailsPage from './ProductDetailsPage';
const Data = ({ item, setClickedProduct}) => {
  const { image, title, id } = item;
  // console.log(item)
 
  // const [clickedItem,setClickedItem] =useState({clickedItem:item});
  const navigate = useNavigate(); // Initialize to null

  const handleUserClick = () => {
    navigate(`/Product/details/${id}`);
    setClickedProduct((item));
    // console.log("Passed",clickedItem);
    // <ProductDetailsPage clickedItem={clickedItem}/>
  };

  return (
    <div className='productDetailContainer' key={item.id} onClick={()=>handleUserClick()}>
      <img src={image} className="productImage" alt={title} />
      <h2 className='titleText'>{title}</h2>
    </div>
  );
};

export default Data;



